# users

A role to manage user accounts.

# Supported Operating Systems

* Raspbian Stretch Lite 2018-10-09
* Debian Stretch

# Requirements

* The username must match the SSH public key part name.
  * For example, if username is `foo` then the key must be `foo.pub`.

# Role Variables

* `users_usernames`: A list of usernames to create accounts for.

* `users_user_groups`: A list of groups to put those users in.
  * Default is `sudo`.

* `users_passwordless_sudo`: Whether or not to enable passwordless sudo.
  * Default is `true`.

* `users_ssh_keys_path`: Where you store your ssh public key parts locally.

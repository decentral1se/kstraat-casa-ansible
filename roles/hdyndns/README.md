# hdyndns

A role to install and configure [homebrew-dyndns].

[homebrew-dyndns]: https://pypi.org/project/hdyndns/

# Supported Operating Systems

* Raspbian Stretch Lite 2018-10-09
* Debian Stretch

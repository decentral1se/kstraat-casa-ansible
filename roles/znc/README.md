# znc

A role to install and configure [znc].

[znc]: https://wiki.znc.in/

# Supported Operating Systems

* Raspbian Stretch Lite 2018-10-09
* Debian Stretch

# Role Variables

* `znc_config`: Path to your ZNC configuration.

* `znc_domain`: Domain your ZNC is accessible by.
  * Before using this variable, you must generate a HTTPS
    certificate with LetsEncrypt for this domain. The role then uses the
    `privkey.pem` and `fullchain.pem` at `/etc/letsencrypt/live/{{ znc_domain }}`.

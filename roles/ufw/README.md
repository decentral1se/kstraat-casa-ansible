# ufw

A role to install and configure [UFW].

[UFW]: https://help.ubuntu.com/community/UFW

# Supported Operating Systems

* Raspbian Stretch Lite 2018-10-09
* Debian Stretch

# Role Variables

* `ufw_rules`: List of firewall rules that will be run.
  * Supported keys: `port`, `policy`, `proto` and `direction`.
  * Default is the empty list.

* `ufw_logging`: The firewall log level.
  * Default is `low`.

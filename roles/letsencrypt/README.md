# letsencrypt

A role to install and configure [certbot] from stretch-backports.

[certbot]: https://certbot.eff.org/

Please note, only the NGINX webserver is supported right now.

`python-certbot-nginx` is installed and runs in fully automated luxury mode:

> sudo certbot --nginx

A crontab for renewal is also configured.

# Supported Operating Systems

* Raspbian Stretch Lite 2018-10-09
* Debian Stretch

# Role Variables

* `letsencrypt_email`: Email for certificate issues.
* `letsencrypt_domains`: Domains to generate certificates for.

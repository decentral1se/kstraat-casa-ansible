# ssh

A role to install and configure SSH.

The configuration follows the guidelines of:

* https://linux-audit.com/audit-and-harden-your-ssh-configuration/

# Supported Operating Systems

* Raspbian Stretch Lite 2018-10-09
* Debian Stretch

# Role Variables

* `ssh_port`: The SSH port to expose.
  * Defaults to `22`.

* `ssh_allowed_users`: The usernames for users that can login.
  * Defaults to `[]` (if not used, will not be set).
  * Don't forget to add your own non-root account if using.

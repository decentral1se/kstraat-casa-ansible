# nginx

A role to install and configure [nginx].

[nginx]: https://nginx.org/

# Supported Operating Systems

* Raspbian Stretch Lite 2018-10-09
* Debian Stretch

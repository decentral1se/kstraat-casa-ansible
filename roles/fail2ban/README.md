# fail2ban

A role to install and configure [fail2ban].

[fail2ban]: https://www.fail2ban.org/wiki/index.php/Main_Page

# Supported Operating Systems

* Raspbian Stretch Lite 2018-10-09
* Debian Stretch

# Role Variables

* `fail2ban_jail_local`: Path to your local `jail.local` configuration.

* `fail2ban_jail_specs`: A list of file paths for your jail specifications.
  * Defaults to `[]` (not used if not specified).
